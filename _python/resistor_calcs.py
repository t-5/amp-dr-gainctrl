#!/usr/bin/python3

import math

ZERODB_RESISTOR = 200000
FEEDBACK_R = 1000
STEPS = 11
FIRST_STEP = 2
STEP_SIZE = 2
N = 96 # E-Range for resistors


### HELPER FUNCTIONS ###################################################################################################

def findNearestER(R_destination):
    global E_RESISTOR_SERIES
    found_R, deviation = 100000, 100000000000
    for E_R in E_RESISTOR_SERIES:
        tmp_deviation = abs(R_destination - E_R)
        if tmp_deviation < deviation:
            deviation = tmp_deviation
            found_R = E_R
    return found_R

def dbToGainFactor(dbValue):
    return pow(10, dbValue / 20.0)

def gainFactorToDb(gain):
    if gain == 0.0:
        return -1000000
    return 20 * math.log10(gain)

### MAIN PROG ##########################################################################################################

# E-series Rs ####################################################################################
E_RESISTOR_SERIES = []
for decade in (10, 100, 1000, 10000, 100000):
    for m in range(0, N):
        factor = round((10 ** m) ** (1 / N), 2)
        E_RESISTOR_SERIES.append(factor * decade)

# calculate steps
steps = []
current_step = FIRST_STEP
for i in range(0, STEPS):
    steps.append(current_step)
    current_step += STEP_SIZE


# calculate resistors
resistors = [ZERODB_RESISTOR]
for step in steps:
    factor = dbToGainFactor(step)
    # target R
    r = FEEDBACK_R / (factor - 1) * 2
    # target R with ZERODB_RESISTOR in parallel
    #
    # r = (rX * ZERODB_RESISTOR) /
    # r * (rX + ZERODB_RESISTOR) = rX * ZERODB_RESISTOR
    # r * rX + r * ZERODB_RESISTOR = rX * ZERODB_RESISTOR
    # r * rX - ZERODB_RESISTOR * rX =
    # rX * (r - ZERODB_RESISTOR) = -r * ZERODB_RESISTOR
    rX = -r * ZERODB_RESISTOR / (r - ZERODB_RESISTOR)
    Rer = findNearestER(rX)
    resistors.append(Rer)


# show found Rs and actual factors ind dB
print ("Found resistor values and actual gain factors")
dbStep = FIRST_STEP - STEP_SIZE
for idx, r in enumerate(resistors):
    if idx == 0:
        r_act = r
    else:
        r_act = (r * ZERODB_RESISTOR) / (r + ZERODB_RESISTOR)
    factor = (FEEDBACK_R + r_act / 2) / r_act * 2
    db = gainFactorToDb(factor)
    if r >= 1000:
        sr = "%6.2fkOhm" % (r / 1000)
    else:
        sr = "%6.2f Ohm" % r
    print("%2ddB:  Rx = %s  =>  %5.2fdB" % (dbStep, sr, db))
    dbStep += STEP_SIZE
